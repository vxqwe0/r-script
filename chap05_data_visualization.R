# chap05_data_visualization

# 차트 데이터 생성
chart_data <- c(305,450, 320, 460, 330, 480, 380, 520) 
names(chart_data) <- c("2016 1분기","2017 1분기","2016 2분기","2017 2분기","2016 3분기","2017 3분기","2016 4분기","2017 4분기")
str(chart_data)
chart_data

# 1. 이산변수 시각화
# - 정수단위로 나누어지는 수(자녀 수, 판매 수)

# (1) 막대차트
{
# 세로 막대 차트
barplot(chart_data,                       # 데이터 입력
        ylim = c(0,600),                  # y축 limit
        main="2016년 vs 2017년 판매현황", # 제목 넣기
        col=rainbow(8))  # 색 넣기

# 가로 막대 차트
barplot(chart_data,
        xlim = c(0,600),                  # xlim으로 변경
        horiz=TRUE,                       # 가로 막대 차트
        main="2016년 vs 2017년 판매현황",
        col=rainbow(length(chart_data)))

# 1행 2열 구조
par(mfrow=c(1,2)) # 1행 2열 그래프 보기 / 그래프 여러 개 보기
VADeaths

row_names <- row.names(VADeaths)
col_names <- colnames(VADeaths)

barplot(VADeaths,
        ylim = c(0,250),
        beside=FALSE,                 # 누적형 막대 그래프
        main="버지니아 사망 비율",
        col=rainbow(nrow(VADeaths)))

barplot(VADeaths,
        beside=TRUE,                  # 병치 막대 그래프
        main="버지니아 사망 비율",
        col=rainbow(nrow(VADeaths)))

# 범례 추가
legend(x=0.5, y=250,
       legend=row_names,
       fill=rainbow(nrow(VADeaths)))
}

# (2) 점 차트
{
dotchart(chart_data,
         color=c("green","red"),
         lcolor="black",
         pch=1:2,                   # 그 수에 해당하는 모양이 있음 / 그거 돌려 씀
         labels=names(chart_data),
         xlab="매출액",
         main="분기별 판매현황 점 차트 시각화",
         cex=1.2)                   # 1 이상은 확대, 미만은 축소
}

# (3) 파이 차트 시각화
{
pie(chart_data,
    labels = names(chart_data),
    border='blue',
    col=rainbow(8),
    cex=1.2)

# (직전에 그린) 차트에 제목 추가
title("2014~2015년도 분기별 매출현황")

table(iris$Species)
pie(table(iris$Species),
    col=rainbow(length(table(iris$Species))),
    main="iris 꽃의 종 빈도수")
}


# 2. 연속변수 시각화
# - 시간, 길이 등의 연속성을 갖는 변수

# 1) 상자 그래프 시각화
{
summary(VADeaths)
boxplot(VADeaths)
# 사분위 수
quantile(VADeaths[,1])
quantile(c(1,3,6,10))
}

# 2) 히스토그램 시각화 : 대칭성 확인
{
hist(iris$Sepal.Width,
     xlab="iris$Sepal.Width",
     col="mistyrose",                    # 흐릿한 장미색
     main="iris 꽃받침 넓이 histogram",
     freq=TRUE,                          # 세로축의 값은 구간 내의 개수(기본값) / Frequeny
#    freq=FALSE,                         # 0~1 사이의 값(밀도값)                / Density
     xlim=c(2.0, 4.5))                   # range(iris$Sepal.Width) == 2.0 ~ 4.4

# 밀도 분포 곡선
# freq = FALSE 상태에서 사용할 것
# freq = TRUE  상태에서 사용시 수 크기 차이 때문에 선이 바닥에 깔릴 수 있음
lines(density(iris$Sepal.Width))
}

# 3) 산점도 시각화
# 형식) plot(x축 값, y축 값)
{
x <- runif(n=15, min=1, max=100)
plot(x) # 인자가 하나인 경우 x값은 index
y <- runif(n=15, min=5, max=120)
plot(x,y) # plot(y~x) 같은 값 / 포뮬라

# col 속성
head(iris, 10)

par(mfrow=c(2,2)) # 2행 2열 차트 그리기
price <- rnorm(10)
plot(price, type="l") # 유형 : 실선
plot(price, type="o") # 유형 : 원형과 실선(원형 통과)
plot(price, type="h") # 직선
plot(price, type="s") # 꺾은선

plot(price, type="o", pch=5) # 빈 사각형
plot(price, type="o", pch=15)# 채워진 마름모
plot(price, type="o", pch=20, col="blue") #color 지정
plot(price, type="o", pch=20, col="orange", cex=1.5) #character expension(확대)
plot(price, type="o", pch=20, col="green", cex=2.0, lwd=3) #lwd : line width

# 만능차트 : plot은 해당 모델에 맞게 그래프를 출력함
methods()

# plot.ts : 시계열 자료 (time series)
WWWusage
plot(WWWusage) # 추세선

# plot.lm : 회귀 모델 (linear model)
install.packages("UsingR")
library(UsingR)

data(galton)
plot(galton)
# 유전학자 갈톤 : '회귀'라는 용어 제안

model <- lm(child ~ parent, data=galton)
plot(model)
}

# 4) 산점도 행렬
{
pairs(iris[-5])

# 꽃의 종별 산점도 행렬
table(iris$Species)
pairs(iris[iris$Species == 'setosa', 1:4])
}

# 5) 차트 파일 저장
{
setwd("C:/ITWILL/2_Rwork/output")

jpeg("iris.jpg", width=720, height=480) # 픽셀 지정 가능
plot(iris$Sepal.Length, iris$Petal.Length, col=iris$Species)
title(main="iris 데이터 테이블 산포도 차트")
dev.off() # 장치 종료

# 이미지 파일 확인
}

#########################
### 3차원 산점도 
#########################
install.packages('scatterplot3d')
library(scatterplot3d)
{
# 꽃의 종류별 분류 
iris_setosa = iris[iris$Species == 'setosa',]
iris_versicolor = iris[iris$Species == 'versicolor',]
iris_virginica = iris[iris$Species == 'virginica',]

# scatterplot3d(밑변, 오른쪽변, 왼쪽변, type='n') # type='n' : 기본 산점도 제외 
d3 <- scatterplot3d(iris$Petal.Length, iris$Sepal.Length, iris$Sepal.Width, type='n')

d3$points3d(iris_setosa$Petal.Length, iris_setosa$Sepal.Length,
            iris_setosa$Sepal.Width, bg='orange', pch=21)

d3$points3d(iris_versicolor$Petal.Length, iris_versicolor$Sepal.Length,
            iris_versicolor$Sepal.Width, bg='blue', pch=23)

d3$points3d(iris_virginica$Petal.Length, iris_virginica$Sepal.Length,
            iris_virginica$Sepal.Width, bg='green', pch=25)
}