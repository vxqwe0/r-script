#################################
## <제8장 연습문제>
################################# 

#01. 다음 조건에 맞게 airquality 데이터 셋의 Ozone과 Wind 변수를 대상으로  
# 다음과 같이 산점도로 시각화 하시오.

#조건1) y축 : Ozone 변수, x축 : Wind 변수 
#조건2) Month 변수를 factor형으로 변경  
#조건3) Month 변수를 이용하여 5개 격자를 갖는 산점도 그래프 그리기

###### airquality 데이터 셋 설명 ######
# datasets 패키지에서 제공
# 대기 오염에 관한 데이터 셋
#######################################

library(lattice)
library(datasets)
head(airquality)
str(airquality)

# 자료형 변환 (transform)
air_df <- transform(airquality, Month=factor(Month))
xyplot(Ozone ~ Wind | factor(Month), data=air_df, layout=c(5,1))


# 02. 서울지역 4년제 대학교 위치 정보(Part-II/university.csv) 파일을 이용하여 레이어 형식으로 시각화 하시오.

# 조건1) 지도 중심 지역 SEOUL, zoom=11
# 조건2) 위도(LAT), 경도(LON)를 이용하여 학교의 포인트 표시
# 조건3) 각 학교명으로 텍스트 표시
# 조건4) 완성된 지도를 "university.png"로 저장하기(width=10.24,height=7.68) 

library(ggmap)
setwd("C:/ITWILL/2_Rwork/Part-II")

university <- read.csv("university.csv")
lon = university$LON
lat = university$LAT
name = university$'학교명'
map <- data.frame(university, lon, lat, university_name)

# 지도정보 생성
seoul <- c(left = 126.85, bottom = 37.35,
           right = 127.25, top = 37.65)

map <- get_stamenmap(seoul, zoom = 11, maptype = 'watercolor')

# (1) 레이어1 : 정적 지도 생성

layer1 = ggmap(map)

# (2) 레이어2 : 지도 위에 포인트
layer2 = layer1 + geom_point(data=map, aes(x = lon, y = lat,
                                          color = factor(name),
                                          size = factor(name)))

# (#) 레이어3 : 지도 위에 텍스트 저장
layer3 = layer2 + geom_text(data = map, aes(x = lon, y = lat+0.01, label = name), size=3)
layer3
