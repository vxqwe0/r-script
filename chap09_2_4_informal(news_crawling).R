# chap09_2_4_informal(news_crawling)

# https://media.daum.net/
# <a href="url">기사 내용</a>

# 1. 패키지 설치
install.packages("httr") # 원격 url 요청
install.packages("XML") # tag -> html 파싱

library(httr)
library(XML)

# 2. URL 요청
url <- "https://media.daum.net"
web <- GET(url)

# 3. html 파싱 (text -> html)
help("htmlTreeParse")
html <- htmlTreeParse(web, useInternalNodes = T,
                      trim = T, encoding = "UTF-8")

root_node <- xmlRoot(html)

# 4. tag 자료 수집 : "//tag[@속성='값']"
news <- xpathSApply(root_node, "//a[@class='link_txt']", xmlValue)
news

news2 <- news[1:59]

# 5. news 전처리
news_sent = gsub('[\n\r\t]', '', news2)        # 이스케이프 제거
news_sent = gsub('[[:punct:]]', '', news_sent) # 문장부호 제거
news_sent = gsub('[[:cntrl:]]', '', news_sent) # 특수문자 제거
news_sent = gsub('[a-zA-Z]', '', news_sent)    # 영문자 제거
news_sent = gsub('\\s+', ' ', news_sent)    # 2자 이상 공백 제거

# 6. 파일 저장하기
setwd("C:/ITWILL/2_Rwork/output")
write.csv(news_sent, 'news_data.csv', row.names = T, quote = F)

news_data <- read.csv('news_data.csv')
head(news_data)
colnames(news_data) <- c('no', 'news_text')
news_text <- news_data$news_text
str(news_text)

# 7. 토픽 분석 -> 단어 구름 시각화(1day)

library(KoNLP)
library(tm)
library(wordcloud)

# 신규 단어
user_dic = data.frame(term=c("펜데믹","코로나19", "타다"), tag='ncn')
buildDictionary(ext_dic='sejong',user_dic=user_dic)

# 여기서부터 복붙 / 전처리 과정은 위에서 했으니 생략 / 사실 에러 이슈 있음
# In tm_map.SimpleCorpus(newsCorpus, removeNumbers) : transformation drops documents

exNouns <- function(x) { 
  paste(extractNoun(as.character(x)), collapse=" ")
}

news_nouns <- sapply(news_text, exNouns)
newsCorpus <- Corpus(VectorSource(news_nouns))

newsCorpusPrepro_term <- TermDocumentMatrix(newsCorpus, control=list(wordLengths=c(4,30)))
newsTerm_df <- as.data.frame(as.matrix(newsCorpusPrepro_term))
newsWordResult <- sort(rowSums(newsTerm_df), decreasing=TRUE)
newsName <- names(newsWordResult)
newsWord.df <- data.frame(word=newsName, freq=newsWordResult)

pal <- brewer.pal(12,"Paired")
windowsFonts(malgun=windowsFont("맑은 고딕"))
wordcloud(newsWord.df$word, newsWord.df$freq, 
          scale=c(5,1), min.freq=2, random.order=F, 
          rot.per=.1, colors=pal, family="malgun")